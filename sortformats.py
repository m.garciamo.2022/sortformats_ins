#|/usr/bin/env python3

"""Ordena una lista de formatos según su nivel de compresión.
Utiliza el algoritmo de inserción"""

import sys

# Lista ordenada de formatos de imagen, de menor a mayor inserción
# (clasificación asumida)
fordered: tuple = ("raw", "bmp", "tiff", "ppm", "eps", "tga",
                   "pdf", "svg", "png", "gif", "jpeg", "webp")


def lower_than(format1: str, format2: str) -> bool:
    """Determina si format1 es menor que format2.
    Devuelve True si format1 es menor, False en caso contrario.
    Un formato es menor que otro si aparece antes en la lista fordered.
    """
    return fordered.index(format1) < fordered.index(format2)


def sort_pivot(formats: list, pivot: int):
    """Ordena el formato pivote, intercambiándolo con el formato
    a su izquierda, hasta que quede ordenado.
    """
    while pivot > 0 and lower_than(formats[pivot], formats[pivot - 1]):
        formats[pivot], formats[pivot - 1] = formats[pivot - 1], formats[pivot]
        pivot -= 1
    return pivot

def sort_formats(formats: list) -> list:
    """Ordena la lista de formatos.
    Devuelve la lista ordenada de formatos, según su posición en fordered.
    Utiliza el algoritmo de inserción.
    """
    for i in range(1, len(formats)):
        sort_pivot(formats, i)
    return formats


def main():
    """Lee los argumentos de la línea de comandos y los imprime ordenados.
    Además, verifica si son formatos válidos utilizando la tupla fordered.
    """
    formats: list = sys.argv[1:]
    for format in formats:
        if format not in fordered:
            sys.exit(f"Formato inválido: {format}")
    sorted_formats: list = sort_formats(formats)
    for format in sorted_formats:
        print(format, end=" ")
    print()


if __name__ == '__main__':
    main()